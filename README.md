# Image PHP

Custom php-fpm image based on php-fpm:8. See https://hub.docker.com/r/rhandke/catbytes_php

# PHP Modules

- Core
- ctype
- curl
- date
- dom
- fileinfo
- filter
- ftp
- hash
- iconv
- json
- libxml
- mbstring
- mysqlnd
- openssl
- pcre
- PDO
- pdo_mysql
- pdo_sqlite
- Phar
- posix
- readline
- Reflection
- session
- SimpleXML
- sodium
- SPL
- sqlite3
- standard
- tokenizer
- xml
- xmlreader
- xmlwriter
- zip
- zlib

# Tests

The test.sh script is meant to be used in conjunction with the php-modules file.
